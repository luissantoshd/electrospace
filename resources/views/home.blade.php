@extends('layouts.app') 
@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    <table id="produtos" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>Referência</th>
                                <th>Nome</th>
                                <th>Data Entrada</th>
                                <th>Data Saída</th>
                                <th>ETA</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                \Carbon\Carbon::setLocale('pt');
                            @endphp
                            @foreach(\App\Produtos::all() as $produto)
                            <tr>
                                <td>{{ $produto->ref }}</td>
                                <td>{{ $produto->nome }}</td>
                                <td>{{ $produto->data_entrada }}</td>
                                <td>{{ $produto->data_saida }}</td>
                                <td style="@if(\Carbon\Carbon::parse($produto->data_saida)->diffInDays(\Carbon\Carbon::now()) < 10 && \Carbon\Carbon::parse($produto->data_saida)->diffInDays(\Carbon\Carbon::now()) > 5) background-color: rgba(255, 255, 0, 0.5); @elseif(\Carbon\Carbon::parse($produto->data_saida)->diffInDays(\Carbon\Carbon::now()) <= 5) background-color: rgba(255, 0, 0, 0.5); @endif">
                                    <strong>
                                        {{ preg_replace('/\D/', ' ', \Carbon\Carbon::now()->diffForHumans(\Carbon\Carbon::parse($produto->data_saida))) }}
                                    </strong>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Nome</th>
                                <th>Data Entrada</th>
                                <th>Data Saída</th>
                                <th>ETA</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection