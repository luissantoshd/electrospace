<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Luís Santos',
            'email' => 'luissantos12345@hotmail.com',
            'password' => bcrypt('maio1996')
        ]);
    }
}
