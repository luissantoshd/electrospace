<?php

use Illuminate\Database\Seeder;

class ProdutosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produtos')->insert([
            'ref' => str_random(random_int(1, 30)),
            'nome' => str_random(random_int(1, 50)),
            'data_entrada' => \Carbon\Carbon::now(),
            'data_saida' => \Carbon\Carbon::now()->addDays(random_int(1, 15))
        ]);
        DB::table('produtos')->insert([
            'ref' => str_random(random_int(1, 30)),
            'nome' => str_random(random_int(1, 50)),
            'data_entrada' => \Carbon\Carbon::now(),
            'data_saida' => \Carbon\Carbon::now()->addDays(random_int(1, 15))
        ]);
        DB::table('produtos')->insert([
            'ref' => str_random(random_int(1, 30)),
            'nome' => str_random(random_int(1, 50)),
            'data_entrada' => \Carbon\Carbon::now(),
            'data_saida' => \Carbon\Carbon::now()->addDays(random_int(1, 15))
        ]);
        DB::table('produtos')->insert([
            'ref' => str_random(random_int(1, 30)),
            'nome' => str_random(random_int(1, 50)),
            'data_entrada' => \Carbon\Carbon::now(),
            'data_saida' => \Carbon\Carbon::now()->addDays(random_int(1, 15))
        ]);
        DB::table('produtos')->insert([
            'ref' => str_random(random_int(1, 30)),
            'nome' => str_random(random_int(1, 50)),
            'data_entrada' => \Carbon\Carbon::now(),
            'data_saida' => \Carbon\Carbon::now()->addDays(random_int(1, 15))
        ]);
        DB::table('produtos')->insert([
            'ref' => str_random(random_int(1, 30)),
            'nome' => str_random(random_int(1, 50)),
            'data_entrada' => \Carbon\Carbon::now(),
            'data_saida' => \Carbon\Carbon::now()->addDays(random_int(1, 15))
        ]);
        DB::table('produtos')->insert([
            'ref' => str_random(random_int(1, 30)),
            'nome' => str_random(random_int(1, 50)),
            'data_entrada' => \Carbon\Carbon::now(),
            'data_saida' => \Carbon\Carbon::now()->addDays(random_int(1, 15))
        ]);
        DB::table('produtos')->insert([
            'ref' => str_random(random_int(1, 30)),
            'nome' => str_random(random_int(1, 50)),
            'data_entrada' => \Carbon\Carbon::now(),
            'data_saida' => \Carbon\Carbon::now()->addDays(random_int(1, 15))
        ]);
        DB::table('produtos')->insert([
            'ref' => str_random(random_int(1, 30)),
            'nome' => str_random(random_int(1, 50)),
            'data_entrada' => \Carbon\Carbon::now(),
            'data_saida' => \Carbon\Carbon::now()->addDays(random_int(1, 15))
        ]);
        DB::table('produtos')->insert([
            'ref' => str_random(random_int(1, 30)),
            'nome' => str_random(random_int(1, 50)),
            'data_entrada' => \Carbon\Carbon::now(),
            'data_saida' => \Carbon\Carbon::now()->addDays(random_int(1, 15))
        ]);
        DB::table('produtos')->insert([
            'ref' => str_random(random_int(1, 30)),
            'nome' => str_random(random_int(1, 50)),
            'data_entrada' => \Carbon\Carbon::now(),
            'data_saida' => \Carbon\Carbon::now()->addDays(random_int(1, 15))
        ]);
        DB::table('produtos')->insert([
            'ref' => str_random(random_int(1, 30)),
            'nome' => str_random(random_int(1, 50)),
            'data_entrada' => \Carbon\Carbon::now(),
            'data_saida' => \Carbon\Carbon::now()->addDays(random_int(1, 15))
        ]);
        DB::table('produtos')->insert([
            'ref' => str_random(random_int(1, 30)),
            'nome' => str_random(random_int(1, 50)),
            'data_entrada' => \Carbon\Carbon::now(),
            'data_saida' => \Carbon\Carbon::now()->addDays(random_int(1, 15))
        ]);
        DB::table('produtos')->insert([
            'ref' => str_random(random_int(1, 30)),
            'nome' => str_random(random_int(1, 50)),
            'data_entrada' => \Carbon\Carbon::parse('2018-06-03'),
            'data_saida' => \Carbon\Carbon::parse('2018-06-03')
        ]);
    }
}
